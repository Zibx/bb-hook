<?php
const OUT_DIR = '/dev/shm/bb-hook-events';

global $repository;

function mapping( $obj ){
    global $repository;
    extract( $obj );

    if( !isset($branch) ){
        throw new Exception( 'no branch specified' );
    }
    if( !isset($raw_node) ){
        throw new Exception( 'no raw_node specified' );
    }
    if( !isset($utctimestamp) ){
        throw new Exception( 'no utctimestamp specified' );
    }

    return "{$repository} {$branch} {$raw_node} {$utctimestamp}";
}
function uniqFileName( $dir, $prefix ){
    $postfix = '';
    while( file_exists( $dir .'/'. $prefix . $postfix ) ){
        $postfix = $postfix == '' ? 1 : $postfix + 1;
    }
    return $prefix . $postfix;
}

if( !file_exists( OUT_DIR ) ){
    mkdir( OUT_DIR, 0666 );
    if( file_exists( OUT_DIR ) ){
        echo 'The directory '. OUT_DIR .' was successfully created.';
    }else{
        die('I cannot create a dir ="(');
    }
}

if( isset( $_GET['debug'] ) ){
    //debug parameter
    $_POST["payload"] = "{\"repository\": {\"website\": \"\", \"fork\": false, \"name\": \"bb hook\", \"scm\": \"git\", \"owner\": \"Zibx\", \"absolute_url\": \"\/Zibx\/bb-hook\/\", \"slug\": \"bb-hook\", \"is_private\": false}, \"truncated\": false, \"commits\": [{\"node\": \"dbd370c69c5c\", \"files\": [{\"type\": \"modified\", \"file\": \"index.php\"}], \"raw_author\": \"Zibx <zibx.mail@gmail.com>\", \"utctimestamp\": \"2014-01-19 17:00:21+00:00\", \"author\": \"Zibx\", \"timestamp\": \"2014-01-19 18:00:21\", \"raw_node\": \"dbd370c69c5c8e16b1e1a9cb58b43a12d2a3d372\", \"parents\": [\"2ebb485e24cd\"], \"branch\": \"master\", \"message\": \"debug\\n2\\n\", \"revision\": null, \"size\": -1}], \"canon_url\": \"https:\/\/bitbucket.org\", \"user\": \"Zibx\"}";
}

if( isset( $_POST['payload']) && $_SERVER['HTTP_USER_AGENT'] === 'Bitbucket.org' ){
    //if we write more than one line in comment => bitbucket generates not valid json, so I have to replace \n.
    $data = json_decode( str_replace( "\n", '', $_POST['payload'] ), true );

    //build url like  git@bitbucket.org:Zibx/bb-hook.git
    $repository = 'git@bitbucket.org:'. $data['repository']['owner'].'/'. $data['repository']['slug'] .'.'. $data['repository']['scm'];

    $outputRows = array_map( "mapping", (array)$data['commits'] );
    $fullFileName = OUT_DIR .'/'. uniqFileName( OUT_DIR, time() );
    $outputData = implode( '\n', $outputRows );

    if( isset( $_GET['debug'] ) ){
        echo 'write to file {$fullFileName}:<br>&nbsp;&nbsp;&nbsp;'. str_replace( "\n", "<BR>\n&nbsp;&nbsp;&nbsp;", $outputData );
    }else{
        file_put_contents(  $fullFileName, $outputData );
    }
}
